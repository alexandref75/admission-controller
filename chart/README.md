# smarter-admission-controller

This chart installs Admission Controller for Triton Inference Server in smarter edge

## About

Triton is an inference server for ML models that supports multiple models frameworks, tensorflow, pytorch and others. Smarter admission controller adds a resource management framework in front of Triton enabling its use on edge nodes.

## Usage

Please check the main [README](https://gitlab.com/smarter-project/admission-controller) for usage models.

```console
helm install smarter-admission-controller chart
```

